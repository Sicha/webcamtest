﻿using UnityEngine;
using UnityEngine.UI;
//using System.Runtime.InteropServices;
//using System;

public class WebCamTextureTest : MonoBehaviour
{
    [Header("WebCam Settings")] 
    [SerializeField] private RawImage webCamPreview;
    [SerializeField] private RawImage scaledRTPreview;
    
    [Header("WebCam Settings")]
    [SerializeField] private int requestedWidth = 1920;
    [SerializeField] private int requestedHeight = 1080;
    [SerializeField] private int requestedFps = 30;

    [Header("Visage Settings")] 
    [SerializeField] private int visageTextureWidth = 640;
    [SerializeField] private int visageTextureHeight = 360;
    [SerializeField] private int isMirrored = 0;
    
    private WebCamTexture webCamTexture;
    private int deviceNum = 0;
    private RenderTexture scaledRt;
    private Texture2D scaledTexture;
    
    // Example Function for sending frame from Unity to Visage plugin 
    //private const string dllName = "__Internal";
    //[DllImport (dllName)]
    //public static extern void _processFrame(IntPtr framePtr, int width, int height, int mirrored, int format, float angle);

    private void Awake()
    {
        for(var i = 0; i < WebCamTexture.devices.Length; i++)
        {
            if (WebCamTexture.devices[i].isFrontFacing)
                deviceNum = i;
        }
    
        // We can stick to one specific format if it will be easier to implement on plugins side
        scaledRt = new RenderTexture(visageTextureWidth, visageTextureHeight, 24, RenderTextureFormat.ARGB32);
        scaledRTPreview.texture = scaledRt;
        
        webCamTexture = new WebCamTexture(WebCamTexture.devices[deviceNum].name, requestedWidth, requestedHeight, requestedFps);
        webCamTexture.Play();
        
        webCamPreview.texture = webCamTexture;
    }

    private void Update()
    {
        if (!webCamTexture.didUpdateThisFrame) return;
        
        // Frame resizing
        Graphics.Blit(webCamTexture, scaledRt);
        
        var ptr = scaledRt.GetNativeTexturePtr();
        // Send data to visage for processing
        //_processFrame(ptr, scaledRt.width, scaledRt.height, isMirrored, (int)scaledRt.format, webCamTexture.videoRotationAngle);
        
        // We also can convert scaledRt to Texture2D if needed
        // BUT this is undesirable, because it will take additional 10-20ms of processing time for resolution 640x360)
        //ConvertToTexture2D();
        //_processFrame(scaledTexture.GetNativeTexturePtr(), scaledTexture.width, scaledTexture.height, isMirrored,
        //      (int)scaledTexture.format, webCamTexture.videoRotationAngle);
        
        
        
        // After sending frame to Visage we can use this functions to get TrackerStatus and update masks
        //VisageTrackerNative._track();
        //VisageTrackerNative._getTrackerStatus(TrackerStatus);
    }

    /// <summary>
    ///  Converting from RenderTexture to Texture2D
    /// </summary>
    private void ConvertToTexture2D()
    {
        if(scaledTexture == null)
            scaledTexture = new Texture2D(scaledRt.width, scaledRt.height, TextureFormat.ARGB32, false);
        
        var prevRT = RenderTexture.active;
        RenderTexture.active = scaledRt;
        scaledTexture.ReadPixels(new Rect(0, 0, scaledRt.width, scaledRt.height), 0, 0);
        scaledTexture.Apply();
        RenderTexture.active = prevRT;
    }
}