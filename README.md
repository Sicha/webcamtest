# Unity WebCamTexture resizing

We made a small example of how Unity WebCamTexture may be used for accessing camera stream and resizing default (1920x1080) frame to lower resolution (640x360) for future sending this frame to Visage plugin. You may use this example for testing of implemented function. You can find everything in WebCamTextureTest.cs.

## Parameters
We expect to have something similar to ```_processFrame``` function to send the frame directly from Unity to Visage plugin.
This function has the next parameters:
* ```IntPtr framePtr``` - pointer to low-resolution texture;
* ```int width``` - width of low-resolution texture;
* ```int height``` - height of low-resolution texture; 
* ```int mirrored``` - same as isMirrored value which Visage currently use in Traker.cs
* ```int format``` - low-resolution texture format (we can stick to any format for iOS and Android if it will easier and faster to implement on plugin side).
* ```float rotation``` - low-resolution texture clockwise rotation. This parameter is good to have, but we can rotate texture in Unity if needed.

## Expected result
We expect that this will help us perform the fast implementation of the next operational sequence:
1. Get initial frame from image/video/camera/etc. on Unity side;
2. Resize to lower resolution (for example 640x360) on Unity side withing fast GPU processing;
3. Send frame to Visage;
4. Use ```_track()``` and ```_getTrackerStatus()``` of already implemented Visage Unity plugin to process sent frame.
